import com.typesafe.config.ConfigFactory
import models.Utils._
import models._

import scala.annotation.tailrec
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.language.implicitConversions
import scala.util.Random

object Main {

  def report[A](
                 title: String, listA: Seq[A], listB: Seq[A], kennwert: A => Double, extraStats: List[(String, Any, Any, Any)],
                 breakEven: Option[Double] = None
               ): Unit = {
    val sortedA = listA.map(kennwert).sorted
    val sortedB = listB.map(kennwert).sorted
    val padding = 17
    println("## " + title)
    val headers = List(
      "",
      "Simulation",
      "Alternative",
      "Differenz"
    )
    val values = extraStats ::: List(
      ("5% Quantil", quantil(5, sortedA), quantil(5, sortedB), quantil(5, sortedB) - quantil(5, sortedA)),
      ("10% Quantil", quantil(10, sortedA), quantil(10, sortedB), quantil(10, sortedB) - quantil(10, sortedA)),
      ("25% Quantil", quantil(25, sortedA), quantil(25, sortedB), quantil(25, sortedB) - quantil(25, sortedA)),
      ("Median", quantil(50, sortedA), quantil(50, sortedB), quantil(50, sortedB) - quantil(50, sortedA)),
      ("Mittelwert", sortedA.sum / sortedA.length, sortedB.sum / sortedB.length, sortedB.sum / sortedB.length - sortedA.sum / sortedA.length),
      ("75% Quantil", quantil(75, sortedA), quantil(75, sortedB), quantil(75, sortedB) - quantil(75, sortedA)),
      ("95% Quantil", quantil(95, sortedA), quantil(95, sortedB), quantil(95, sortedB) - quantil(95, sortedA))
    ) ++ (breakEven match {
      case None => Nil
      case Some(value) =>
        val evenPercentA = 100.0 * sortedA.count(_ <= value) / sortedA.length
        val evenPercentB = 100.0 * sortedB.count(_ <= value) / sortedB.length
        List(("Unter Inflation",
          String.format("%.2f%%", evenPercentA),
          String.format("%.2f%%", evenPercentB),
          String.format("%.2f%%", evenPercentB - evenPercentA)))
    })

    println(prettyTable(headers, values))
  }

  def runBatch(runs: Int, seed: Long): Unit = {
    val simulationA = Simulation(
      jahre = p_jahre,
      erwarteteJahresrendite = p_rendite,
      erwarteteAusschuettungsrendite = p_ausRendite,
      useHistorical = p_useHistorical,
      basiszins = p_basiszins,
      risiko = p_risiko,
      sparrate = p_sparrate,
      einmalanlage = p_einmalanlage,
      seed = seed,
      feeCalculator = p_gebuehr,
      runs = runs,
      threads = p_threads
    )

    val simulationB = p_compareParameter match {
      case "ausschuettung" | "aus" =>
        val other = p_compareValue match {
          case percentPattern(percent) => percent.toDouble / 100.0
          case _ => p_compareValue.toDouble
        }
        simulationA.copy(erwarteteAusschuettungsrendite = other)
      case "gebuehr" | "fees" =>
        val other = p_compareValue match {
          case "none" => FeeCalculator.NoFees
          case "ing" => FeeCalculator.INGFees
          case "tr" => FeeCalculator.TradeRepublicFees
        }
        simulationA.copy(feeCalculator = other)
      case "risiko" =>
        val other = p_compareValue match {
          case percentPattern(percent) => percent.toDouble / 100.0
          case _ => p_compareValue.toDouble
        }
        simulationA.copy(risiko = other)
      case "rendite" =>
        val other = p_compareValue match {
          case percentPattern(percent) => percent.toDouble / 100.0
          case _ => p_compareValue.toDouble
        }
        simulationA.copy(erwarteteJahresrendite = other)
      case "basiszins" =>
        val other = p_compareValue match {
          case percentPattern(percent) => percent.toDouble / 100.0
          case _ => p_compareValue.toDouble
        }
        simulationA.copy(basiszins = other)
      case s =>
        println("Vergleich von Parameter nicht unterstützt: " + p_compareParameter)
        simulationA.copy()
    }


    val simulationInflation = simulationB.copy(
      erwarteteJahresrendite = p_inflation,
      erwarteteAusschuettungsrendite = 0.0,
      risiko = 0,
      useHistorical = "",
      feeCalculator = FeeCalculator.NoFees,
      noTax = true,
      runs = 1
    )

    val resultInflation = Await.result(simulationInflation.execute(), Duration.Inf).head
    println("Run Simulation A: \n" + simulationA.toPrettyString)
    val resultsA = Await.result(simulationA.execute(), Duration.Inf)
    println("Run Simulation B: \n" + simulationB.toPrettyString)
    val resultsB = Await.result(simulationB.execute(), Duration.Inf)

    val eingezahlt = resultInflation.eingezahltTotal
    val nachInflation = resultInflation.wert

    println("################# ERGEBNIS #################")
    printf("Eingezahlt     : %11.2f\n", eingezahlt)
    printf("Nach Inflation : %11.2f\n", nachInflation)

    if (runs == 1) {
      println("# Ergebnis Original")
      println(resultsA.head.toPrettyString)
      printf("Rendite p.a. %2.2f%%", resultsA.head.kursentwicklung.mittlereJahresrendite(p_jahre) * 100.0 - 100.0)
      printf(" , Volatilitaet: %2.2f%%", resultsA.head.kursentwicklung.vola(p_jahre))
      println()

      println("# Ergebnis Alternative")
      println(resultsB.head.toPrettyString)
      printf("Rendite p.a. %2.2f%%", resultsB.head.kursentwicklung.mittlereJahresrendite(p_jahre) * 100.0 - 100.0)
      printf(" , Volatilitaet: %2.2f%%", resultsB.head.kursentwicklung.vola(p_jahre))
      println()
    } else {
      val abschluesseA = resultsA.map(_.jahresabschluesse.head)
      val abschluesseB = resultsB.map(_.jahresabschluesse.head)

      report[Jahresabschluss]("Wert", abschluesseA, abschluesseB, _.wert, List(("Eingezahlt", eingezahlt, eingezahlt, "")))
      println()
      report[Jahresabschluss]("Netto Wert", abschluesseA, abschluesseB, _.nettowert, List(("Eingezahlt", eingezahlt, eingezahlt, "")),
        Some(nachInflation))
      println()
      report[Double](
        "Renditen",
        resultsA.map(_.kursentwicklung.mittlereJahresrendite(p_jahre) * 100 - 100),
        resultsB.map(_.kursentwicklung.mittlereJahresrendite(p_jahre) * 100 - 100),
        identity,
        Nil,
        None
      )
      println()
      report[Double](
        "Volas",
        resultsA.map(_.kursentwicklung.vola(p_jahre)),
        resultsB.map(_.kursentwicklung.vola(p_jahre)),
        identity,
        Nil,
        None
      )
      //      printf("mittlere Rendite A: %.2f%%\n", resultsA.map(_.kursentwicklung.mittlereJahresrendite(p_jahre)).sum / runs * 100 - 100)
      //      printf("mittlere Rendite B: %.2f%%\n", resultsB.map(_.kursentwicklung.mittlereJahresrendite(p_jahre)).sum / runs * 100 - 100)
      //      printf("mittlere Vola A: %.2f%%\n", resultsA.map(_.kursentwicklung.vola(p_jahre)).sum / runs)
      //      printf("mittlere Vola B: %.2f%%\n", resultsB.map(_.kursentwicklung.vola(p_jahre)).sum / runs)
    }
  }

  val batchPattern = """(\d*[km]?)""".r
  val setParameterPattern = """(\S*?)\s*[= ]\s*(\S*)""".r
  val comparePattern = """compare\s+(\S+)\s+(\S+)""".r

  val percentPattern = """(\d+\.?\d*)%""".r

  var p_sparrate: Int = _
  var p_jahre: Int = _
  var p_inflation: Double = _
  var p_einmalanlage: Int = _
  var p_rendite: Double = _
  var p_useHistorical: String = _
  var p_ausRendite: Double = _
  var p_risiko: Double = _
  var p_gebuehr: FeeCalculator = _
  var p_basiszins: Double = _
  var p_threads: Int = _
  var p_seed: Long = _
  var p_compareParameter: String = _
  var p_compareValue: String = _

  def resetParameters(): Unit = {
    val config = ConfigFactory.load()

    p_sparrate = config.getInt("defaults.sparrate")
    p_jahre = config.getInt("defaults.jahre")
    p_inflation = config.getDouble("defaults.inflation")
    p_einmalanlage = config.getInt("defaults.einmalanlage")
    p_rendite = config.getDouble("defaults.rendite")
    p_ausRendite = config.getDouble("defaults.ausschuettungsrendite")
    p_risiko = config.getDouble("defaults.risiko")
    p_useHistorical = ""
    p_gebuehr = FeeCalculator.INGFees
    p_basiszins = config.getDouble("defaults.basiszins")
    p_threads = config.getInt("defaults.threads")
    p_seed = 0L
    p_compareParameter = "ausschuettung"
    p_compareValue = "0.0"

    printParameters()
  }

  def printParameters(): Unit = {
    println("Current Parameters:")
    val values = List(
      "sparrate" -> p_sparrate,
      "jahre" -> p_jahre,
      "inflation" -> String.format("%.2f%%", p_inflation * 100),
      "einmalanlage" -> p_einmalanlage,
      "rendite" -> String.format("%.2f%%", p_rendite * 100),
      "hsitorische kurse" -> p_useHistorical,
      "ausschuettungsrendite" -> String.format("%.2f%%", p_ausRendite * 100),
      "risiko" -> String.format("%.2f%%", p_risiko * 100),
      "gebuehren" -> p_gebuehr.name,
      "basiszins" -> String.format("%.2f%%", p_basiszins * 100),
      "threads" -> p_threads,
      "seed" -> p_seed,
      "compare parameter" -> p_compareParameter,
      "compare value" -> p_compareValue
    )

    println(prettyTable(List("Parameter", "Wert"), values))
  }

  def parseNumberString(s: String): Option[Int] = {
    s match {
      case "" => None
      case s if s.endsWith("k") => Some((s.stripSuffix("k").toDouble * 1000).toInt)
      case s if s.endsWith("m") => Some((s.stripSuffix("m").toDouble * 1000000).toInt)
      case s => Some(s.toInt)
    }
  }

  @tailrec
  def startCLI(): Unit = {
    System.gc()
    print("Press ENTER to run a single simulation or type an integer to run a batch. Type 'exit' to exit.\n > ")
    Console.in.readLine().trim match {
      case "reset" =>
        resetParameters()
        println("parameters reset to default")
        startCLI()
      case comparePattern(in_param, in_val) =>
        p_compareParameter = in_param
        p_compareValue = in_val
        println("Now Comparing: " + p_compareParameter)
        printParameters()
        startCLI()
      case setParameterPattern(parameter, value) =>
        try {
          parameter match {
            case "seed" =>
              p_seed = value.toLong
              println(parameter + " = " + value)
            case "sparrate" | "rate" =>
              p_sparrate = parseNumberString(value).getOrElse(0)
              println(parameter + " = " + value)
            case "jahre" =>
              p_jahre = parseNumberString(value).getOrElse(1)
              println(parameter + " = " + value)
            case "einmalanlage" | "einmal" =>
              p_einmalanlage = parseNumberString(value).getOrElse(0)
              println(parameter + " = " + value)
            case "rendite" =>
              value match {
                case percentPattern(percent) => p_rendite = percent.toDouble / 100.0
                case _ => p_rendite = value.toDouble
              }
              println(parameter + " = " + value)
            case "ausschuettung" | "aus" =>
              value match {
                case percentPattern(percent) => p_ausRendite = percent.toDouble / 100.0
                case _ => p_ausRendite = value.toDouble
              }
              println(parameter + " = " + value)
            case "risiko" =>
              value match {
                case percentPattern(percent) => p_risiko = percent.toDouble / 100.0
                case _ => p_risiko = value.toDouble
              }
              println(parameter + " = " + value)
            case "historisch" | "historical" =>
              p_useHistorical = value
              println(parameter + " = " + value)
            case "basiszins" =>
              value match {
                case percentPattern(percent) => p_basiszins = percent.toDouble / 100.0
                case _ => p_basiszins = value.toDouble
              }
              println(parameter + " = " + value)
            case "gebühr" | "gebuehr" | "fee" | "fees" =>
              value match {
                case name if name.equalsIgnoreCase("ING") =>
                  p_gebuehr = FeeCalculator.INGFees
                case "no" | "none" | "zero" =>
                  p_gebuehr = FeeCalculator.NoFees
                case _ => println("There is no fee calculator for: " + value)
              }
              println(parameter + " = " + p_gebuehr.name)
            case "threads" =>
              p_threads = value.toInt
              println(parameter + " = " + value)
            case "inflation" =>
              value match {
                case percentPattern(percent) => p_inflation = percent.toDouble / 100.0
                case _ => p_inflation = value.toDouble
              }
              println(parameter + " = " + value)
            case _ => println("no such parameter: " + parameter)
          }
        } catch {
          case e: Throwable => e.printStackTrace()
        }
        printParameters()
        startCLI()
      case "exit" | "quit" | "q" =>
        println("good bye")
      case batchPattern(in_runs) =>
        val runs = parseNumberString(in_runs).getOrElse(1)

        val seed = if (p_seed == 0L) Random.nextLong() else p_seed

        runBatch(runs, seed)

        startCLI()
      case input: String =>
        println("command unknown: " + input)
        startCLI()
    }
  }

  def main(args: Array[String]): Unit = {
    resetParameters()

    startCLI()

    Simulation.actorSystem.terminate()
  }
}
