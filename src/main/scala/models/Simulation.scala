package models

import akka.actor.{ActorSystem, Props}

import scala.annotation.tailrec
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random

case class Simulation(
                       jahre: Int,
                       erwarteteJahresrendite: Double,
                       erwarteteAusschuettungsrendite: Double,
                       risiko: Double,
                       useHistorical: String,
                       basiszins: Double,
                       einmalanlage: Int,
                       sparrate: Int,
                       seed: Long,
                       feeCalculator: FeeCalculator,
                       runs: Int,
                       threads: Int,
                       noTax: Boolean = false,
                       lowMemory: Boolean = false
                     )(implicit ec: ExecutionContext) {

  @tailrec
  private def simulate(monate: Int, random: Random, result: ETF = null): ETF = {
    monate match {
      case 0 =>
        result.kursentwicklung.clearAndDisableCache()
        if (lowMemory) result.copy(buyTransactions = Vector.empty, sellTransactions = Vector.empty)
        else result
      case _ if result == null =>
        val list = useHistorical match {
          case "acwi" => Kursentwicklung.msciAcwiHistory
          case "world" => Kursentwicklung.msciWorldHistory
          case "em" => Kursentwicklung.msciEMHistory
          case "" => Nil
          case s =>
            println("No History available for: " + s)
            println("Fallback to Random")
            Nil
        }

        simulate(monate, random,
          ETF(
            ausschuettungsrendite = erwarteteAusschuettungsrendite,
            basiszins = basiszins,
            einmalanlage,
            feeCalculator,
            Kursentwicklung(random.nextLong(), erwarteteJahresrendite, risiko, erwarteteAusschuettungsrendite, list),
            noTax = noTax
          )
        )
      case _ => simulate(monate - 1, random, result.nextMonth(sparrate))
    }
  }

  def execute(): Future[Seq[ETF]] = {
    if (runs > 1)
      batchRun(threads, runs)
    else
      Future.successful(List(simulate(jahre * 12, new Random(seed))))
  }

  private def batchRun(threads: Int, runs: Int): Future[Seq[ETF]] = {
    val random = new Random(seed)
    val runsPerThread = runs / threads
    val progress = Simulation.actorSystem.actorOf(Props(classOf[Progress], runs))
    val start = System.currentTimeMillis()
    val workers = (1 to threads) map { _ =>
      val threadRandom = new Random(random.nextLong())
      Future {
        for (n <- 1 to runsPerThread) yield {
          if (n % 100 == 0) progress ! 100
          simulate(jahre * 12, threadRandom)
        }
      }
    }
    Future.sequence(workers).map(_.flatten).andThen { _ =>
      val end = System.currentTimeMillis()
      val duration = end - start
      printf("Finished: %d:%02d:%02d.%-2ds\n",
        duration / (1000 * 60 * 60),
        (duration / (1000 * 60)) % 60,
        ((duration / 1000) % 60) % 60,
        ((duration % 1000) % 60) % 60
      )
    }
  }

  def toPrettyString: String = {
    val hist = if (useHistorical != "") List("historisch" -> useHistorical) else Nil
    (List(
      "jahre" -> jahre.toString,
      "rendite" -> erwarteteJahresrendite,
      "ausschuettung" -> erwarteteAusschuettungsrendite,
      "risiko" -> risiko,
      "einmalanlage" -> einmalanlage.toString,
      "sparrate" -> sparrate.toString,
      "seed" -> seed.toString,
      "fee calculator" -> feeCalculator.name,
      "runs" -> runs.toString,
      "threads" -> threads,
      "noTax" -> noTax
    ) ::: hist).map({ case (a, b) => "- " + a.padTo(18, ' ') + ": " + b })
      .mkString("\n")
  }
}

object Simulation {
  val actorSystem: ActorSystem = ActorSystem("SimulationActorSystem")
}