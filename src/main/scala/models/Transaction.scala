package models

case class Transaction(
                        monat: Int,
                        handelskurs: Float,
                        number: Float,
                        fee: Float
                      )
