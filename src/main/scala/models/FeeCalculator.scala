package models

case class FeeCalculator(
                          name: String,
                          feeOnceConst: Double,
                          feeOnceRel: Double,
                          feePlanConst: Double,
                          feePlanRel: Double
                        ) {
  def feeBruttoOnce(brutto: Double): Double = if (brutto <= 0) 0 else (feeOnceConst + feeOnceRel * brutto) / (1 + feeOnceRel)

  def feeBruttoPlan(brutto: Double): Double = if (brutto <= 0) 0 else (feePlanConst + feePlanRel * brutto) / (1 + feePlanRel)

  def feeNettoOnce(netto: Double): Double = if (netto <= 0) 0 else feeOnceConst + netto * feeOnceRel
}

object FeeCalculator {

  object INGFees extends FeeCalculator("ING DiBa", 4.9, 0.00025, 0.0, 0.0175)

  object TradeRepublicFees extends FeeCalculator("Trade Republic", 1.0, 0.0, 0.0, 0.0)

  object NoFees extends FeeCalculator("No Fees", 0.0, 0.0, 0.0, 0.0)

}

