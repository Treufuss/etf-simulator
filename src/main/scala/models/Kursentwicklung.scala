package models

import scala.annotation.tailrec
import scala.util.Random

case class Kursentwicklung(
                            seed: Long,
                            erwarteteJahresrendite: Double,
                            risiko: Double,
                            ausschuettungsrendite: Double,
                            sampleKurse: Seq[Double] = Nil
                          ) {

  def kurs(monat: Int): Double = kursgeschichte(monat).head._1

  def ausschuettung(monat: Int): Double = kursgeschichte(monat).head._2

  private var cacheEnabled: Boolean = true
  private var cache: Vector[(Double, Double)] = Vector.empty

  def clearAndDisableCache(): Unit = this.synchronized {
    cacheEnabled = false
    cache = Vector.empty
  }

  private def kursgeschichte(monat: Int): Vector[(Double, Double)] = this.synchronized {
    // https://en.wikipedia.org/wiki/Geometric_Brownian_motion
    // https://youtu.be/sfCrU5awVmw
    val dt = 1.0 / 12
    val r = math.log(1 + erwarteteJahresrendite)
    val sigma = risiko * 3

    val shift = new Random(seed).nextInt(math.max(sampleKurse.length, 1))

    @tailrec
    def noMemoryKursgeschichteRec(
                                   zielMonat: Int,
                                   rand: Random = new Random(seed),
                                   letzteKurse: Vector[(Double, Double)] = Vector(100.0 -> 0.0),
                                   monat: Int = 1
                                 ): Vector[(Double, Double)] = {
      if (monat > zielMonat) letzteKurse
      else {
        val kursNeu = {
          val faktor = sampleKurse match {
            case Nil => math.exp((r - 1 / 2 * sigma * sigma) * dt - sigma * rand.nextGaussian() * dt)
            case _ =>
              val index = ((shift + monat) % (sampleKurse.length - 1)) + 1
              sampleKurse(index) / sampleKurse(index - 1)
          }
          val wertNeu = letzteKurse.head._1 * faktor
          if (ausschuettungsrendite > 0.0 && monat % 12 == 0) {
            val ausschuettung = wertNeu * ausschuettungsrendite
            (wertNeu * (1 - ausschuettungsrendite), ausschuettung)
          } else (wertNeu, 0.0)
        }

        noMemoryKursgeschichteRec(zielMonat, rand, kursNeu +: letzteKurse, monat + 1)
      }
    }

    if (cacheEnabled) {
      if (cache.size <= monat) {
        val newSize = math.max(cache.size * 2, monat + 1)
        cache = noMemoryKursgeschichteRec(newSize)
      }
      cache.drop(cache.size - monat - 1)
    } else noMemoryKursgeschichteRec(monat)
  }

  def jahresrendite(jahr: Int): Double = kurs(jahr * 12) / kurs((jahr - 1) * 12) - 1

  def alleKurse(zeitraumJahre: Int): Seq[Double] = kursgeschichte(zeitraumJahre * 12).map(_._1)

  def mittlereJahresrendite(zeitraumJahre: Int): Double = math.pow(kurs(zeitraumJahre * 12) / 100, 1.0 / zeitraumJahre)

  def vola(zeitraumJahre: Int): Double = {
    val hist = kursgeschichte(zeitraumJahre * 12).zipWithIndex.filter(_._2 % 12 == 0).map(_._1._1)
    val a = hist.drop(1)
    val b = hist.dropRight(1)
    val renditen = a.zip(b)
      .map(t => t._2 / t._1)
      .map(_ * 100.0 - 100.0)
    val bezugswert = renditen.sum / renditen.length
    math.sqrt(renditen
      .map(g => (g - bezugswert) * (g - bezugswert))
      .sum / zeitraumJahre)
  }

  def apply(monat: Int): Double = kurs(monat)
}

object Kursentwicklung {
  lazy val msciWorldHistory: Seq[Double] = try {
    val source = scala.io.Source.fromFile("src/main/resources/msci_world_history.csv")
    val out = source.getLines()
      .map(_.toDouble)
      .toVector
    source.close()
    out
  } catch {
    case _: Throwable => Nil
  }

  lazy val msciAcwiHistory: Seq[Double] = try {
    val source = scala.io.Source.fromFile("src/main/resources/msci_acwi_history.csv")
    val out = source.getLines()
      .map(_.toDouble)
      .toVector
    source.close()
    out
  } catch {
    case _: Throwable => Nil
  }

  lazy val msciEMHistory: Seq[Double] = try {
    val source = scala.io.Source.fromFile("src/main/resources/msci_em_history.csv")
    val out = source.getLines()
      .map(_.toDouble)
      .toVector
    source.close()
    out
  } catch {
    case _: Throwable => Nil
  }
}
