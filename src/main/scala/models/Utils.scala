package models

object Utils {

  def quantil(percent: Int, list: Seq[Double]) = {
    list.drop((list.size * percent / 100.0).toInt).head
  }

  def geomMittel(list: Seq[Double]): Double = {
    math.exp(list.map(math.log).sum / list.length)
  }

  def prettyTable(headers: Seq[String], values: Seq[Product]): String = {
    val stringValues = values map { row =>
      row.productIterator.toSeq map {
        case value: Int => value.toString
//        case value: Float if math.abs(value) < 1 => String.format("%.2f%%", value * 100)
        case value: Float => String.format("%.2f", value)
//        case value: Double if math.abs(value) < 1 => String.format("%.2f%%", value * 100)
        case value: Double => String.format("%.2f", value)
        case value => value.toString
      }
    }

    val maxLengths = (headers.map(_.length) +: stringValues
      .map(_.map(_.length)))
      .reduce((l, r) => l.zip(r).map(t => math.max(t._1, t._2)))

    val paddedHeaders = headers
      .zip(maxLengths)
      .map({ case (header, length) => header.padTo(length, ' ') })

    val separators = maxLengths.map(length => "-" * length)

    val paddedValues = stringValues.map(_.zip(maxLengths).map({ case (value, length) => String.format(s"%${length}s", value) }))

    val lines = (paddedHeaders +: separators +: paddedValues) map { row =>
      row.map(" " + _ + " ").mkString("|", "|", "|")
    }

    lines.mkString("\n")
  }

}
