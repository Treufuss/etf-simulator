package models

case class Jahresabschluss(
                            jahr: Int,
                            kurs: Float,
                            anteile: Float,
                            eingezahltTotal: Float,
                            wert: Float,
                            gebuehren: Float,
                            ausschuettung: Float,
                            vorabpauschale: Float,
                            vorabpauschaleTotal: Float,
                            steuern: Float,
                            nettowert: Float
                          )
