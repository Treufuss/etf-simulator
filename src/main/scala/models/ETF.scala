package models

import com.typesafe.config.ConfigFactory

import scala.annotation.tailrec
import scala.language.implicitConversions

case class ETF(
                monat: Int,
                ausschuettungsrendite: Float,
                basiszins: Float,
                vorabpauschaleTotal: Float,
                eingezahltTotal: Float,
                anteile: Float,
                buyTransactions: Vector[Transaction],
                sellTransactions: Vector[Transaction],
                wertJahresanfang: Float,
                jahresabschluesse: Vector[Jahresabschluss],
                feeCalculator: FeeCalculator,
                kursentwicklung: Kursentwicklung,
                noTax: Boolean
              ) {

  import ETF.doubleToFloat
  import feeCalculator._

  def activeBuyTransactions: Vector[Transaction] = {
    val soldTotal = math.abs(sellTransactions.map(_.number).fold(0f)(_ + _))

    @tailrec
    def rec(buys: Vector[Transaction], remain: Double): Vector[Transaction] = {
      if (remain <= 0) buys
      else {
        if (buys(0).number < remain)
          rec(buys.drop(1), remain - buys(0).number)
        else {
          buys.updated(0, buys(0).copy(number = buys(0).number - remain))
        }
      }
    }

    rec(buyTransactions.reverse, soldTotal).reverse
  }

  lazy val wert: Double = anteile * kursentwicklung(monat)

  lazy val nettoWert: Double = {
    val eingezahlt = activeBuyTransactions.map(t => t.number * t.handelskurs).fold(0f)(_ + _)

    val gewinn = wert - eingezahlt - vorabpauschaleTotal

    val nachSteuer = if (gewinn > 0 && !noTax)
      wert - gewinn * 0.7 * ETF.abgeltungssteuersatz
    else wert

    nachSteuer - feeNettoOnce(nachSteuer)
  }

  def buy(anzahl: Double, fee: Double): ETF = {
    val transaction = Transaction(monat, kursentwicklung(monat), anzahl, fee)
    this.copy(buyTransactions = transaction +: buyTransactions, anteile = anteile + anzahl)
  }

  def buyOnce(brutto: Double): ETF = {
    val fee = feeBruttoOnce(brutto)
    val anzahl = (brutto - fee) / kursentwicklung(monat)
    buy(anzahl, fee)
  }

  def buyPlan(brutto: Double): ETF = {
    val fee = feeBruttoPlan(brutto)
    val anzahl = (brutto - fee) / kursentwicklung(monat)
    buy(anzahl, fee).copy(eingezahltTotal = eingezahltTotal + brutto)
  }

  def sell(anzahl: Double, fee: Double): ETF = {
    val transaction = Transaction(monat, kursentwicklung(monat), -anzahl, fee)
    this.copy(sellTransactions = transaction +: sellTransactions, anteile = anteile - anzahl)
  }

  def sellOnce(netto: Double): ETF = {
    val fee = feeNettoOnce(netto)
    val anzahl = (netto + fee) / kursentwicklung(monat)
    sell(anzahl, fee)
  }

  def nextMonth(sparrate: Double): ETF = {
    if (monat % 12 == 11) {
      this.copy(monat = monat + 1)
        .buyPlan(sparrate)
        .jahresabrechnung()
    } else {
      this.copy(monat = monat + 1)
        .buyPlan(sparrate)
    }
  }

  private def jahresabrechnung(): ETF = {

    val gewinn = (wert - buyTransactions
      .filter(_.monat > monat - 12)
      .map(t => t.handelskurs * t.number)
      .sum) - wertJahresanfang

    val ausschuettung =
      if (ausschuettungsrendite > 0.0) kursentwicklung.ausschuettung(monat) * anteile else 0

    val wertsteigerung = gewinn - ausschuettung

    val vorabpauschale = if (wertsteigerung > 0) {
      val basisertrag = wertJahresanfang * 0.7 * basiszins
      if (basisertrag > ausschuettung) {
        if (gewinn > basisertrag)
          basisertrag - ausschuettung
        else
          wertsteigerung
      } else 0
    } else 0

    val steuern =
      if (noTax) 0.0
      else math.max((ausschuettung * 0.7 + vorabpauschale - ETF.pauschbetrag) * ETF.abgeltungssteuersatz, 0)

    var updated = this

    if (ausschuettung >= steuern) {
      // wiederanlage
      val wiederanlage = ausschuettung - steuern
      if (wiederanlage > 1)
        updated = updated.buyOnce(wiederanlage)
      val wertJahresende = updated.wert
      updated = updated.copy(wertJahresanfang = wertJahresende)
    } else {
      // verkauf zur steuerzahlung
      val schuld = steuern - ausschuettung
      updated = updated.sellOnce(schuld)
      val wertJahresende = updated.wert
      updated = updated.copy(wertJahresanfang = wertJahresende)
    }

    updated.copy(
      vorabpauschaleTotal = vorabpauschaleTotal + vorabpauschale,
      jahresabschluesse = Jahresabschluss(
        jahr = monat / 12,
        kurs = kursentwicklung.kurs(updated.monat),
        anteile = updated.anteile,
        eingezahltTotal = updated.eingezahltTotal,
        gebuehren = (updated.buyTransactions ++ updated.sellTransactions).filter(_.monat > monat - 12).map(_.fee).sum,
        wert = updated.wert,
        ausschuettung = ausschuettung,
        vorabpauschale = vorabpauschale,
        vorabpauschaleTotal = updated.vorabpauschaleTotal + vorabpauschale,
        steuern = steuern,
        nettowert = updated.nettoWert
      ) +: jahresabschluesse)
  }

  def toPrettyString: String = {
    val headers = Vector(
      "jahr",
      "kurs",
      "+-%",
      "anteile",
      "eingezahlt",
      "gebuehren",
      "ausschuettung",
      "vorabpauschale",
      "vorab. Total",
      "steuer",
      "wert",
      "netto wert"
    )
    val values = jahresabschluesse.reverse map { abschluss =>
      val kursgewinn = kursentwicklung.jahresrendite(abschluss.jahr) match {
        case gewinn if gewinn >= 0 => String.format("+%.2f%%", gewinn * 100)
        case verlust => String.format("-%.2f%%", -verlust * 100)
      }
      (
        abschluss.jahr,
        abschluss.kurs,
        kursgewinn,
        abschluss.anteile,
        abschluss.eingezahltTotal,
        abschluss.gebuehren,
        abschluss.ausschuettung,
        abschluss.vorabpauschale,
        abschluss.vorabpauschaleTotal,
        abschluss.steuern,
        abschluss.wert,
        abschluss.nettowert
      )
    }
    Utils.prettyTable(headers, values)
  }

}

object ETF {

  implicit def doubleToFloat(d: Double): Float = d.toFloat

  private val config = ConfigFactory.load()

  val abgeltungssteuersatz: Double = config.getDouble("abgeltungssteuersatz")
  val pauschbetrag: Double = config.getDouble("pauschbetrag")

  def apply(
             ausschuettungsrendite: Double,
             basiszins: Double,
             einmalanlage: Double,
             feeCalculator: FeeCalculator,
             kursentwicklung: Kursentwicklung,
             noTax: Boolean
           ): ETF = {
    val fee = feeCalculator.feeBruttoOnce(einmalanlage)
    new ETF(
      monat = 0,
      ausschuettungsrendite = ausschuettungsrendite,
      basiszins = basiszins,
      vorabpauschaleTotal = 0.0,
      eingezahltTotal = einmalanlage,
      anteile = (einmalanlage - fee) / 100,
      buyTransactions = Vector(Transaction(
        monat = 0,
        handelskurs = 100,
        number = (einmalanlage - fee) / 100,
        fee = fee
      )),
      sellTransactions = Vector.empty,
      wertJahresanfang = einmalanlage - fee,
      jahresabschluesse = Vector(Jahresabschluss(
        jahr = 0,
        kurs = 100.0,
        anteile = (einmalanlage - fee) / 100,
        eingezahltTotal = einmalanlage,
        gebuehren = fee,
        wert = einmalanlage - fee,
        ausschuettung = 0,
        vorabpauschale = 0,
        vorabpauschaleTotal = 0,
        steuern = 0,
        nettowert = einmalanlage - fee - feeCalculator.feeNettoOnce(einmalanlage - fee)
      )),
      feeCalculator = feeCalculator,
      kursentwicklung = kursentwicklung,
      noTax = noTax
    )
  }
}