package models

import akka.actor.Actor

class Progress(goal: Int) extends Actor {

  override def receive: Receive = onMessage(0, 0, System.currentTimeMillis())

  private def onMessage(sum: Int, lastSum: Int, time: Long): Receive = {
    case i: Int =>
      if (System.currentTimeMillis() >= time + 1000) {
        printf("%4.1f%% - %d sims/s\n", 100.0 * sum / goal, sum - lastSum)
        context.become(onMessage(sum + i, sum, System.currentTimeMillis()))
      } else context.become(onMessage(sum + i, lastSum, time))

  }
}
