import models.{FeeCalculator, Kursentwicklung, Simulation}
import org.scalatest.funsuite.AnyFunSuite

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.util.Random

class Tests extends AnyFunSuite {

  test("Kursentwicklung") {
    val kursentwicklung = Kursentwicklung(
      seed = 0L,
      risiko = 0.4,
      erwarteteJahresrendite = 0.07,
      ausschuettungsrendite = 0.0
    )

    println("30 jahre rendite p.a.: " + (kursentwicklung.mittlereJahresrendite(30) * 100 - 100))
    println("30 jahre vola: " + kursentwicklung.vola(30))
//    println()
//    println(kursentwicklung.alleKurse(30).reverse.mkString("\n"))
  }

  test("seeding") {
    val simulation1 = Simulation(
      seed = 0L,
      einmalanlage = 10000,
      sparrate = 250,
      erwarteteJahresrendite = 0.07,
      erwarteteAusschuettungsrendite = 0.0,
      useHistorical = "",
      basiszins = 0.003,
      feeCalculator = FeeCalculator.INGFees,
      jahre = 20,
      risiko = 0.4,
      runs = 1000,
      threads = 2
    )

    val simulation2 = simulation1.copy()

    val resultsFut = for {
      res1 <- simulation1.execute()
      res2 <- simulation2.execute()
    } yield (res1, res2)

    val (result1, result2) = Await.result(resultsFut, Duration.Inf)

    println(result1.head.toPrettyString)
    println(result2.head.toPrettyString)

    assert(result1 == result2)
  }

  test("risiko -> vola") {
    val random = new Random()
    val iterations = 1000

    val result = Range(0, 100, 10) map { risiko =>
      random.setSeed(0L)
      val adjustiert = risiko
      (risiko, adjustiert) -> (1 to iterations).map({ _ =>
        Kursentwicklung(
          seed = random.nextLong(),
          risiko = adjustiert / 100.0,
          erwarteteJahresrendite = 0.07,
          ausschuettungsrendite = 0.0
        )
      }).map(_.vola(100)).sum / iterations
    }

    println(result.mkString("\n"))
  }

  test("msci history") {
    val x = Kursentwicklung(
      seed = 3L,
      erwarteteJahresrendite = 0.0,
      ausschuettungsrendite = 0.0,
      risiko = 0.0,
      sampleKurse = Kursentwicklung.msciWorldHistory
    )

    println(x.alleKurse(1).mkString("\n"))
    println(x.alleKurse(1).mkString("\n"))
  }

  test("Kauf/Verkauf") {

    val simulation = Simulation(
      jahre = 10,
      erwarteteJahresrendite = 0.07,
      erwarteteAusschuettungsrendite = 0.0,
      risiko = 0.0,
      useHistorical = "",
      basiszins = 0.01,
      einmalanlage = 100000,
      sparrate = 100,
      seed = 1L,
      feeCalculator = FeeCalculator.INGFees,
      runs = 1,
      threads = 1
    )

    val etf = Await.result(simulation.execute(), Duration.Inf).head

    println(etf.toPrettyString)
  }

  test("batch 100000") {
    Main.resetParameters()
    Main.runBatch(100000, 1L)
  }
}
